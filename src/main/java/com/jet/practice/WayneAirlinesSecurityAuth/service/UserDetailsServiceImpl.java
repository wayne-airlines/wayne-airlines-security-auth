package com.jet.practice.WayneAirlinesSecurityAuth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jet.practice.WayneAirlinesSecurityAuth.entity.User;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private BCryptPasswordEncoder encoder;

	//TODO make this db call. 
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		String uri = "http://localhost:6970/api/login/user/";
		RestTemplate restTemplate = new RestTemplate();
		User user = restTemplate.getForObject(uri + username, User.class);
		user.setPassword(encoder.encode(user.getPassword()));
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_" + user.getRole().toUpperCase());
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				grantedAuthorities);
	}
}
